from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from configbot import TOKEN

bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands='help')
async def cmd_test1(message: types.Message):
    await message.reply('Пока что этот бот может возвращать текст,который написал пользователь.В будущем будет добавлено что-то еще')


@dp.message_handler()
async def echo_message(msg: types.Message):
    print(f'{msg.from_user} {msg.text}')
    await bot.send_message(msg.from_user.id, msg.text)

if __name__ == '__main__':
    executor.start_polling(dp)
