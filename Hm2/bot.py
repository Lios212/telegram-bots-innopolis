"""
Реализовать запрос на на отправку копии сообщения пользователю, который отправил его боту.

https://gitlab.com/Lios212/telegram-bots-innopolis
"""

from config import TOKEN

from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram import Bot, types

ADMIN = [884167279]

bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


@dp.message_handler()
async def msg_replay(msg: types.Message):
    await msg.reply(msg.text)

if __name__ == '__main__':
    executor.start_polling(dp)

